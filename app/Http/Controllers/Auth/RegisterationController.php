<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Redirect;

class RegisterationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    public function registerView(){
          return view('regitration');   }


    public function register(Request $request){
    // hash password
      $isUserExist = Customer::checkIfAgentExist($request->email);
      if(count($isUserExist) > 0){
       return view('regitration',['message' => 'Email already exist']);
      }else{
       $customer = new Customer;
       $password = Hash::make(trim($request->password));
       $user = $customer->addNewCustomer($request,$password);

       return redirect(action('Auth\LoginController@viewLoginForUser'));
      }
    }

}
