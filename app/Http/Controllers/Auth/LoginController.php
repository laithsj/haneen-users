<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\Request;
use Redirect;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function viewLoginForUser(){
        return view('login');
    }

    public function viewLogin(Request $request){
        // if(!auth()->guard()->check()){
        //     return view('login');
        // }
        // return redirect(action('bookController@viewbook'));
    }

    public function login(Request $request){
        // check if email is exist and passowrd matched
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect(action('bookController@viewbook'));
        }else{
            return redirect(action('Auth\LoginController@viewLoginForUser'))->withErrors(['Invalid Email or Password']);
        }

    }
    public function logout(){
        Auth::logout();
    		session()->forget('web_logged_in');
    		session()->forget('web');
    		return Redirect::to(action('Auth\LoginController@viewLoginForUser'));
	}
}
