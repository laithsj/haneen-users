<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $table = 'customer';

    protected $fillable = [
        'name', 'email', 'password','status',
    ];

    public static function checkIfAgentExist($email){
        return self::where('email',$email)->get();
    }

    public function addNewCustomer($request,$password){
        $this->name     =   $request->name;
        $this->email    =   $request->email;
        $this->password =   $password;
        $this->save();

        return $this;
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


}
