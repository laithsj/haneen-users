$(document).ready(function() {
    $('#register').click(function(evt){
        if($('#Password').val() != $('#confirmPassword').val()) {
            evt.preventDefault();
            $("#NotValid").css("display", "block");
            $("#NotValid").text("Password and Confirm Password don't match");
            // Prevent form submission
        }else if($('#Password').val() == $('#confirmPassword').val()) {
          $("#NotValid").css("display", "none");
          $("#NotValid").text("");
          $('#registrationForm').submit();
        }
    });
});
