<!DOCTYPE html>
<html lang="en">
<head>
    <title>Travalers &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="StyleSheet" type="text/css" href="css/users.css">
    <link rel="StyleSheet" type="text/css" href="css/book.css">
    <link rel="StyleSheet" type="text/css" href="css/home.css">
</head>
<body>
<header class="bg-light-header">
    <div class="container">
         <div class="row">
             <div class="col-lg-10 col-5 ">
                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                    <a class="navbar-brand " href="{{action('HomeController@ViewHomePage')}}">
                    <img src="">Happy Travels</a>
                    <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarText"
                                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon" aria-hidden="true"></span>
                    </button>    
                 <div class="collapse navbar-collapse" id="navbarText">
                            <ul class="navbar-nav mr-auto" role="list">
                                <li class="nav-item active" role="listitem">
                                <a class="nav-link" href="{{action('HomeController@ViewHomePage')}}"title="Home">
                                            Home
                                <span class="sr-only">(current)</span> </a>
                                </li>
                                <li class="nav-item active" role="listitem">
                                    <a class="nav-link" href="{{action('AboutUsController@ViewAboutUs')}}"title="About Us">About Us</a>
                                </li>
                                <li class="nav-item active" role="listitem">
                                    <a class="nav-link" href="{{action('ContactController@ViewContact')}}"title="contact us">Contact Us</a>
                                </li>
                        </div>
                </nav>
             </div>
            <div class="col-lg-2 col-2">
            <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{action('Auth\LoginController@logout')}}">
                                    <i class="fa fa-fw fa-power-off"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
            </div> 
   </div>
</header>
    <div class="site-section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-7 mb-5">
                    <form action="#" class="p-5 bg-white">
                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="text-black" for="full name">Full Name</label>
                                <input type="text" id="fname" class="form-control" required placeholder="Full Name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="email">Email</label>
                                <input type="email" id="email" class="form-control" required placeholder="Email">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="text-black" for="Phone Number">Phone Number</label>
                                <input type="tel" id="telname" class="form-control" required placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="count of pepole want to book vacation">How Many
                                    Person</label>
                                <select name="count" id="count" class="form-control">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5+</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="text-black" for="Passport Number">Passport Number</label>
                                <input type="text" id="PassportName" class="form-control" required
                                    placeholder="Passport Number">
                            </div>
                            <div class="col-md-6 ">
                                <div class="row form-group mt-2">
                                    <div class="col-md-12">
                                        <label style="font-size: 12px;"> Payment Way :</label>
                                    </div>
                                    <div class="col-4">
                                        <a href="#" title="payment way is visa "> <img src="images/visa.png"class="img-fluid ml-3" alt="visa card"></a>
                                    </div>
                                    <div class="col-4">
                                        <a href="#" title="payment way is master card "> <img src="images/master.jpg " alt="master card"class="img-fluid"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="note">Notes</label>
                                <textarea name="note" id="note" cols="30" rows="5" class="form-control"
                                    placeholder="Write your notes or questions here..."
                                    aria-label="Write your notes or questions here..."></textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Send" title="send button to confirmation book"
                                    class="btn btn-primary py-2 px-4 text-white">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="p-4 mb-3 bg-white">
                        <img src="images/hero_bg_1.jpg" alt="Image" class="img-fluid mb-4 rounded">
                        <details>
                            <summary class="h5 text-black mb-3">More Info</summary>
                            <!-- No need for aria-label attribute-->
                            <p> </p>
                            <p>
                            </p>
                        </details>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer style="background-color:#809fff!important" >
        <div class="container  mt-3">
            <div class="row mt-5">
                <div class="col-md-5 col-6 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold contact text-white mt-5">Happy travel </h6>
                <hr class="mt-0 ">
                <div class="text-white">
                many people  have difficulties finding the best company For tourism in terms of prices and quality of services,
                we can help you finding the good and the honest companies .
                </div>
                </div>
                <div class="col-md-3 col-3 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold contact text-white mt-5 pl-3">Happy travel Links</h6>
                    <hr class="mt-0 text-black">
                    <div style="text-align:center">
                    <a class="text-white "  href="{{action('Auth\RegisterationController@registerView')}}">
                            Regisration            </a>
                   </div>
                    <br>
                    <div style="text-align:center">
                    <a  class="text-white mt-4 mb-4" href="{{action('Auth\LoginController@viewLoginForUser')}}">
                        Log In
                   </a>
                   </div>
                   <br>
                   <div style="text-align:center">

                     <a  class="text-white"href="{{action('SearchController@viewSearch')}}">
                         search
                  </a>
                </div>
                </div>
                
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                    <h6 class="text-uppercase font-weight-bold contact text-white mt-5">Contact</h6>
                    <hr class="mt-0 text-black">
                    <a class="text-white">
                        <i class="fas fa-envelope mt-2 mr-3"></i> <p class="sr-only">emial for happy travel</p>HappyTravel@info.com
                    </a>
                    <br>
                    <a  class="text-white">
                        <i class="fas fa-phone mt-2 mr-3"></i> <p class="sr-only">phone number for happy travel</p>+ 01 234 567 88
                   </a>
                   <br>
                     <a  class="text-white">
                        <i class="fas fa-print mt-2 mr-3"></i> <p class="sr-only">fax  for happy travel</p>+ 01 234 567 89
                  </a>
                </div>
            </div>
        </div>
        </div>
   </footer>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>
</body>

</html>