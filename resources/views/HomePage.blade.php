<!DOCTYPE html>
<html>
<head>
    <title>Haapy Travels</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/main1.css" />
    <link rel="stylesheet" href="css/happytripway.css" />
    <!-- <link rel="stylesheet" href="css/boostrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-aw/all.min.css">
    <link rel="stylesheet" href="css/font-aw/solid.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link rel="StyleSheet" type="text/css" href="css/home.css">
</head>
<body>
<header class="bg-light-header">
    <div class="container">
         <div class="row">
             <div class="col-lg-10 col-5 ">
                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                    <a class="navbar-brand " href="{{action('HomeController@ViewHomePage')}}">
                    <img src="">Happy Travels</a>
                    <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarText"
                                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon" aria-hidden="true"></span>
                    </button>
                 <div class="collapse navbar-collapse" id="navbarText">
                            <ul class="navbar-nav mr-auto" role="list">
                                <li class="nav-item active" role="listitem">
                                <a class="nav-link" href="{{action('HomeController@ViewHomePage')}}"title="Home">
                                            Home
                                <span class="sr-only">(current)</span> </a>
                                </li>
                                <li class="nav-item active" role="listitem">
                                    <a class="nav-link" href="{{action('AboutUsController@ViewAboutUs')}}"title="About Us">About Us</a>
                                </li>
                                <li class="nav-item active" role="listitem">
                                    <a class="nav-link" href="{{action('ContactController@ViewContact')}}"title="contact us">Contact Us</a>
                                </li>
                        </div>
                </nav>
             </div>
            <div class="col-lg-2 col-2">
                    <button class="nav-link log_in" data-toggle="modal" data-target="#myModal"style="border: 0px !important; margin-top:6px!important"aria-haspopup="true">
                        <i class="fa fa-power-off fa-xs" aria-hidden="true"></i>
                        Log in
                    </button>
                    <div id="myModal" class="modal fade margin " role="dialog" >
                        <div class="modal-dialog" >
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" title="modal header">
                                    <div class="col-12">
                                        <h4 class="modal-title"  autofocus >Log In As </h4>
                                    </div>
                                </div>
                                <div class="modal-body mt-5 mb-3">
                                    <div class="row">
                                        <div class="offset-md-2 col-md-3  justify-content-center">
                                            <a href="{{action('Auth\LoginController@viewLoginForUser')}}" class="users">
                                                <img src="images/users.png"
                                                    class="img-fluid" alt="User avatar">
                                                <br>
                                                <span class="user"> User </span>
                                            </a>
                                        </div>
                                        <div class=" offset-md-1 col-md-3  justify-content-center">
                                            <a href="https://agents.happytravel.com" class="users">
                                                <img src="images/agent.jpg"
                                                    class="img-fluid" alt="User avatar">
                                                <br>
                                                <span class="user agent"> Agent </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
   </div>
</header>
    <div class="searchsection ">
            <div class="container center inner " style=" padding-bottom:80px;">
                <h1 class="searchcontent">CHECK AND BOOK YOUR HAPPY VACATION</h1>
                <form method="get" action="{{action('SearchController@viewSearch')}}" autocomplete="off">
                    <div class="row" id="searchPart1">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="btn-group btn-group-lg" role="group">
                                <button type="button" id="localTripSearch" class="btn btn-light black-text btn2"><i
                                        class="fa fa-info-circle" data-toggle="tooltip" data-placement="bottom"
                                        title="Domestic Vacation is an option to search for Domestic travel agents that provide Domestic trips in one country only, trips can includes stay in some cities in the target country and  the trip period."></i>
                                    Domestic Trip</button>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="card localcard" id="localTripSearchPart">
                        <div class="card-body">
                            <div class="row uniform d-flex justify-content-start">
                                <div class="col-md-3">
                                    <input name="country" required  autofocus type="text" class="blackplace" placeholder="country">
                                </div>
                                <div class="col-md-3">
                                <span class="sr-only">Please choice the date for search trip betweent from date to todate  </span>
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="sr-only">Please chose the date by adding day / month / year</span>
                                            <input type="text"required id="datepicker1" class="datepicker" placeholder="1/1/2018" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="sr-only">Please chose the date by adding day / month / year</span>
                                            <input type="text" required id="datepicker2" class="datepicker" placeholder="1/1/2019" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="submit" value="Search For Agent" class="search" />
                                </div>
                            </div>
                        </div>
                 </div>
                </form>
                <form method="get" action="{{action('SearchController@viewSearch')}}" autocomplete="off">
                    <div class="row" id="searchPart1">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="btn-group btn-group-lg " role="group">
                            <button type="button" id="globleTripSearch" class="btn btn-secondary btn2"><i
                                        class="fa fa-info-circle" data-toggle="tooltip" data-placement="bottom"
                                        title="International vacation is an option to search for International travel agents that provide International trips from country to another which is includes hotels reservation and visites to populer places to the target country."></i>
                                    International Trip</button>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                    <div class="card globalcard" id="globleTripSearchPart">
                        <div class="card-body" style="">
                            <div class="row uniform">
                                <div class="col-md-3">
                                    <input   required id="fromDest"name="fromDest" type="text" autofocus class="blackplace" oninput="setCustomValidity('')" oninvalid="setCustomValidity('please choise the country you want to travel from')" placeholder="from">
                                </div>
                                <div class="col-md-3">
                                    <input   type="text"   name="toDest"  class="blackplace" oninput="setCustomValidity('')" oninvalid="setCustomValidity('please choise the country you want to travel to')"  required placeholder="to">
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="datepicker1"required name="dateFrom"  class="datepicker" placeholder="1/1/2019" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="datepicker2"required name="dateTo" class="datepicker" placeholder="1/1/2019" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" value="Search For Trip" class="search" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
           </div>
    </div>
     <section id="three" class="wrapper special container">
        <div class="inner">
            <header class="align-center">
                <h2 class="popular">Populer Destinations</h2>
            </header>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="image fit">
                        <a href="">
                            <div class="image-container overlay">
                                <img src="images/sydney.jpg" alt=" Sydney Pictuer " /> <!-- full word that will be good  -->
                                <div class="image-text">Sydney</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="image fit">
                        <a href="">
                            <div class="image-container overlay">
                                <img src="images/london.jpg" alt="landon Pictuer " />
                                <div class="image-text">London</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="image fit">
                        <a href="">
                            <div class="image-container overlay">
                                <img src="images/paris.jpg" alt="paris Pictuer" />
                                <div class="image-text">Paris</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="image fit">
                        <a href="">
                            <div class="image-container overlay">
                                <img src="images/newyork.jpg" alt=" New York Pictuer" />
                                <div class="image-text">New York</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="image fit">
                        <a href="">
                            <div class="image-container overlay">
                                <img src="images/istanboul.jpg" alt=" Istanbul Pictuer " />
                                <div class="image-text">Istanbul</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <footer style="background-color:#809fff!important" >
        <div class="container  mt-3">
            <div class="row mt-5">
                <div class="col-md-5 col-6 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold contact text-white mt-5">Happy travel </h6>
                <hr class="mt-0 ">
                <div class="text-white">
                many people  have difficulties finding the best company For tourism in terms of prices and quality of services,
                we can help you finding the good and the honest companies .
                </div>
                </div>
                <div class="col-md-3 col-3 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold contact text-white mt-5 pl-3">Happy travel Links</h6>
                    <hr class="mt-0 text-black">
                    <div style="text-align:center">
                    <a class="text-white "  href="{{action('Auth\RegisterationController@registerView')}}">
                            Regisration            </a>
                   </div>
                    <br>
                    <div style="text-align:center">
                    <a  class="text-white mt-4 mb-4" href="{{action('Auth\LoginController@viewLoginForUser')}}">
                        Log In
                   </a>
                   </div>
                   <br>
                   <div style="text-align:center">

                     <a  class="text-white"href="{{action('SearchController@viewSearch')}}">
                         search
                  </a>
                </div>
                </div>

                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                    <h6 class="text-uppercase font-weight-bold contact text-white mt-5">Contact</h6>
                    <hr class="mt-0 text-black">
                    <a class="text-white">
                        <i class="fas fa-envelope mt-2 mr-3"></i> <p class="sr-only">emial for happy travel</p>HappyTravel@info.com
                    </a>
                    <br>
                    <a  class="text-white">
                        <i class="fas fa-phone mt-2 mr-3"></i> <p class="sr-only">phone number for happy travel</p>+ 01 234 567 88
                   </a>
                   <br>
                     <a  class="text-white">
                        <i class="fas fa-print mt-2 mr-3"></i> <p class="sr-only">fax  for happy travel</p>+ 01 234 567 89
                  </a>
                </div>
            </div>
        </div>
        </div>
   </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/home.js"></script>
</body>

</html>
