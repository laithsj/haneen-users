<!DOCTYPE html>
<html lang="en">
<head>
    <title> Happy Travales</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/aboutUS.css">
    <link rel="StyleSheet" type="text/css" href="css/home.css">
    <link rel="StyleSheet" type="text/css" href="css/users.css">
</head>
<body>
<header class="bg-light-header">
    <div class="container">
         <div class="row">
             <div class="col-lg-10 col-5 ">
                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                    <a class="navbar-brand " href="{{action('HomeController@ViewHomePage')}}">
                    <img src="">Happy Travels</a>
                    <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarText"
                                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon" aria-hidden="true"></span>
                    </button>    
                 <div class="collapse navbar-collapse" id="navbarText">
                            <ul class="navbar-nav mr-auto" role="list">
                                <li class="nav-item active" role="listitem">
                                <a class="nav-link" href="{{action('HomeController@ViewHomePage')}}"title="Home">
                                            Home
                                <span class="sr-only">(current)</span> </a>
                                </li>
                                <li class="nav-item active" role="listitem">
                                    <a class="nav-link" href="{{action('AboutUsController@ViewAboutUs')}}"title="About Us">About Us</a>
                                </li>
                                <li class="nav-item active" role="listitem">
                                    <a class="nav-link" href="{{action('ContactController@ViewContact')}}"title="contact us">Contact Us</a>
                                </li>
                        </div>
                </nav>
             </div>
            <div class="col-lg-2 col-2">
                    <button class="nav-link log_in" data-toggle="modal" data-target="#myModal"style="border: 0px !important; margin-top:6px!important">
                        <i class="fa fa-power-off fa-xs" aria-hidden="true"></i>
                        Log in
                    </button>
                    <div id="myModal" class="modal fade margin " role="dialog" >
                        <div class="modal-dialog" >
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" title="modal header">
                                    <div class="col-12">
                                        <h4 class="modal-title" >Log In As</h4>
                                    </div>
                                </div>
                                <div class="modal-body mt-5 mb-3">
                                    <div class="row">
                                        <div class="offset-md-2 col-md-3  justify-content-center">
                                            <a href="{{action('Auth\LoginController@viewLoginForUser')}}" class="users">
                                                <img src="images/users.png"
                                                    class="img-fluid" alt="user picture">
                                                <br>
                                                <span class="user"> User </span>
                                            </a>
                                        </div>
                                        <div class=" offset-md-1 col-md-3  justify-content-center">
                                            <a href="#" class="users">
                                                <img src="images/agent.jpg"
                                                    class="img-fluid" alt="Agent picture">
                                                <br>
                                                <span class="user agent"> Agent </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
   </div>
</header>
    <div class="site-blocks-cover inner-page-cover"  data-aos="fade"
        data-stellar-background-ratio="0.5" >
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-black ">ABOUT</h1>
                    <div><a href="home.html" class="text-white">Home</a>
                     <span class="mx-2 text-black">&bullet;</span> 
                     <span class="text-black">About</span></div>

                </div>
            </div>
        </div>
    </div>
    <div class="site-section" data-aos="fade-up">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12 pl-md-5 ">
                    <h2 class="font-weight-light text-black mb-4 about_website">About Website</h2> <!--No need for aria-label in here I delete it, the reader will read it without title attribute  -->
                    <p class="text-black " > <!--No need for title in here I delete it, the reader will read it without title attribute  -->
                        many people want to book with a tour operator that organizes a schedule for
                        some
                        activities in the country they want to visit but might have difficulties finding the best
                        company
                        For tourism in terms of prices and  quality of services, we can help you find it .<br>    
                        we can help you  finding the good and the  honest companies  so 
                        we decided to create a website that
                        allows
                        tourism companies to register on it  and offer their services-booking internal and external trip -  to the  customers.<br> 
                        its  also provides the opportunity for people wishing to travel abroad or to
                        make
                        domestic tourist trips to get more offers and better services from more than one company
                        and
                        compare them and choose the right in terms of price and quality.

                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="site-section block-13 bg-light">
            <div class="container" data-aos="fade">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7">
                        <h2 class="font-weight-light text-black text-center" >What
                            People Says</h2> <!--No need for aria-label in here I delete it, the reader will read it without title attribute  -->
                    </div>
                </div>

                <div class="nonloop-block-13 owl-carousel">
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 mb-5" aria-hidden="true">
                                    <img src="images/img_1.jpg"  class="img-md-fluid">
                                </div>
                                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                                    <p class="text-black lead"></p>
                                    <p class="">&mdash; <em>
                                        My experience was fairly good, like what I expected
                                    </em> <span> Traveler</span></p> <!-- I added this title attribute -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 mb-5">
                                    <img src="images/img_2.jpg" aria-hidden="true" class="img-md-fluid">
                                </div>
                                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                                    <p class="text-black lead"></p>
                                    <p class="">&mdash; <em>
                                        I liked the site's ability to deal with different users and to suit their ability
                                    </em> <span> Traveler</span></p> <!-- I added this title attribute -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 mb-5"aria-hidden="true">
                                    <img src="images/img_4.jpg" alt="Image" class="img-md-fluid">
                                </div>
                                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                                    <p class="text-black lead"></p>
                                    <p class="">&mdash; <em>
                                        I liked the efficiency of the tourist offices that I deal with
                                    </em>, <span> Traveler</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-section border-top">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h2 class="mb-5 text-black" aria-label="Want To Travel With Us?">Want To Travel With Us?
                        </h2>
                        <p class="mb-0"><a href="{{action('Auth\LoginController@login')}}" class="btn btn-primary py-3 px-5 text-white"
                                title="book button for users who want to book vacation">Book
                                Now</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer style="background-color:#809fff!important" >
        <div class="container  mt-3">
            <div class="row mt-5">
                <div class="col-md-5 col-6 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold contact text-white mt-5">Happy travel </h6>
                <hr class="mt-0 ">
                <div class="text-white">
                many people  have difficulties finding the best company For tourism in terms of prices and quality of services,
                we can help you finding the good and the honest companies .
                </div>
                </div>
                <div class="col-md-3 col-3 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold contact text-white mt-5 pl-3">Happy travel Links</h6>
                    <hr class="mt-0 text-black">
                    <div style="text-align:center">
                    <a class="text-white "  href="{{action('Auth\RegisterationController@registerView')}}">
                            Regisration            </a>
                   </div>
                    <br>
                    <div style="text-align:center">
                    <a  class="text-white mt-4 mb-4" href="{{action('Auth\LoginController@viewLoginForUser')}}">
                        Log In
                   </a>
                   </div>
                   <br>
                   <div style="text-align:center">

                     <a  class="text-white"href="{{action('SearchController@viewSearch')}}">
                         search
                  </a>
                </div>
                </div>
                
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                    <h6 class="text-uppercase font-weight-bold contact text-white mt-5">Contact</h6>
                    <hr class="mt-0 text-black">
                    <a class="text-white">
                        <i class="fas fa-envelope mt-2 mr-3"></i> <p class="sr-only">emial for happy travel</p>HappyTravel@info.com
                    </a>
                    <br>
                    <a  class="text-white">
                        <i class="fas fa-phone mt-2 mr-3"></i> <p class="sr-only">phone number for happy travel</p>+ 01 234 567 88
                   </a>
                   <br>
                     <a  class="text-white">
                        <i class="fas fa-print mt-2 mr-3"></i> <p class="sr-only">fax  for happy travel</p>+ 01 234 567 89
                  </a>
                </div>
            </div>
        </div>
        </div>
   </footer>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main2.js"></script>

</body>


</html>