<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/main1.css" />
    <link rel="stylesheet" href="css/happytripway.css" />
    <!-- <link rel="stylesheet" href="css/boostrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-aw/all.min.css">
    <link rel="stylesheet" href="css/font-aw/solid.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link rel="StyleSheet" type="text/css" href="css/registration.css">
</head>
<body class="login-page">
    <div class="container">
        <div class="row ">
            <div class="col-12 d-flex justify-content-end">
                <div class="col-lg-5 col-7  login-box mb-3">
                    <p class="register  mt-2" aria-label="Register a new membership">Register a new membership</p>
                    <form  action="{{action('Auth\RegisterationController@register')}}" method="post" id="registrationForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                            <input type="text" for="name" name="name" class="form-control"  pattern="[a-zA-Z ]*" oninput="setCustomValidity('')" oninvalid="setCustomValidity('Please enter on alphabets only. ')" id="name" required placeholder="Full name" >
                            <span class="glyphicon glyphicon-user "></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="email" for="email"  name="email" class="form-control" required placeholder="Email">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" for="password" name="password" class="form-control" required placeholder="Password"pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" for="Retype password" class="form-control" required placeholder="Retype password">
                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        </div>
                        <div class="row">
                        <div class="col-12">
                        <div class="checkbox ">
                                           <label> </label>
                                            <input type="checkbox" required class="mt-3"> I agree to the <a href="#">terms</a>

                                        </div>

                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="row">
                            <div class="col-lg-12 col-6 mt-3 d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" aria-label="Register" id="register">Register</button>
                            </div>
                            @if(isset($message) && $message != '')
                                <div class="row">
                                   <div class="col-lg-12 d-flex justify-content-center">
                                     <p class="btn btn-danger"> {{$message}} !</p>
                                   <div>
                                </div>
                                </div>
                            @endif

                            <!-- /.col -->
                            <div class="mb-2 mt-2">
                                <a href="{{action('Auth\LoginController@viewLoginForUser')}}" class="text-center end_section" aria-label="I already have a membership ">I already have acount</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
        <script src="{{env('http:ent.happytravel.com//ag')}}/js/validation.js?rb=1"></script>
    </body>
</html>
