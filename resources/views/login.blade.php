<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/main1.css" />
    <link rel="stylesheet" href="css/happytripway.css" />
    <!-- <link rel="stylesheet" href="css/boostrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-aw/all.min.css">
    <link rel="stylesheet" href="css/font-aw/solid.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link rel="stylesheet" href="css/log_in.css ">
</head>
<body class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-end ">
                    <div class="col-lg-5 col-7  login-box">
                        <p class="sign">Sign In</p>
                        <form action="{{action('Auth\LoginController@login')}}" method="post">
                          {{ csrf_field() }}
                            <div class="form-group ">
                                <input type="email" for="email" class="form-control" placeholder="Email" required name="email">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" for="password" class="form-control" placeholder="Password" required name="password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            @if($errors->any())
                            <div class="row">
                                <div class="col-lg-12 mb-3 d-flex justify-content-center">
                                    <div class="card bg-danger text-white shadow">
                                        <div class="card-body "style="padding:5px 17px!important;">
                                            Opps!
                                        <div class="text-white small">
                                        {{$errors->first()}}
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-lg-4 col-6">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat" aria-label="sign in"> Sign
                                        In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                        <div class="col-12 sign_in_data">
                            <a href="{{action('Auth\ForgotPasswordController@ForgotPasswordView')}}" class="end_section" aria-label="I forgot my password">I forgot my password</a><br>
                            <a href="{{action('Auth\RegisterationController@registerView')}}" class="text-center end_section "aria-label="Register a new acount">Register  new acount</a>
                        </div>
                    </div>
                    <!-- /.login-box-body -->
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
