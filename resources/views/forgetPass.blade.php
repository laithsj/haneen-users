<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/main1.css" />
    <link rel="stylesheet" href="css/happytripway.css" />
    <!-- <link rel="stylesheet" href="css/boostrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-aw/all.min.css">
    <link rel="stylesheet" href="css/font-aw/solid.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link rel="stylesheet" href="css/forgetPass.css ">
</head>

<body>

    <div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-password-image">
                                <img src="images/forget.png" width="100%" height="100%">
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 mb-2" >Forgot Your Password?</h1> <!--No need for aria-label in here I delete it, the reader will read it without title attribute  -->
                                        <p class="mb-4">We get it, stuff happens. Just enter your email address below
                                            and
                                            we'll send you a link to reset your password!</p> <!--No need title in here I delete it, the reader will read it without title attribute  -->
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="email" for="email" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address...">
                                        </div>
                                        <a href="#" class="btn btn-primary btn-user btn-block" title="reset password">
                                            Reset Password
                                        </a>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small end_section" href="{{action('Auth\RegisterationController@registerView')}}" title="Create an Account!">Create an Account!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small end_section" href="{{action('Auth\LoginController@login')}}" title="Already have an account? Login!">Already have an account? Login!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>


</html>