<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// login
Route::get('/login',['uses' => 'Auth\LoginController@viewLoginForUser']);
Route::post('/login',['uses' => 'Auth\LoginController@login']);
//logout
Route::get('/logout',['uses' => 'Auth\LoginController@logout']);
// register
Route::get('/register',['uses' => 'Auth\RegisterationController@registerView']);
Route::post('/register',['uses' => 'Auth\RegisterationController@register']);


//forgetpassword
Route::get('/forgetPassword',['uses' =>'Auth\ForgotPasswordController@ForgotPasswordView']);
Route::post('/forgetPassword',['uses' =>'Auth\ForgotPasswordController@ForgotPassword']);
//about us
Route::get('/aboutwebsite',['uses' =>'AboutUsController@ViewAboutUs']);
//search
Route::get('/search',['uses' =>'SearchController@search']);
Route::post('/search',['uses' =>'SearchController@viewSearch']);
//book
Route::get('/Book',['uses' =>'bookController@viewbook']);
//Home Page
Route::get('/Home',['uses' =>'HomeController@ViewHomePage']);
//Contact
Route::get('/contact',['uses' =>'ContactController@ViewContact']);
